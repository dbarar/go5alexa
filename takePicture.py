from flask import Flask
from flask_ask import Ask, statement, question, session
import json
import requests
import time
import unidecode
from random import randint

import cv2

app = Flask(__name__)
ask = Ask(app, "/")

def take_picture():
    cap = cv2.VideoCapture(0)
    ret, frame = cap.read()
    name = 'img_' + time.strftime("%d_%m_%Y") + '_' + time.strftime("%I_%M_%S") + '.bmp'
    cv2.imwrite(name, frame)
    cap.release()

@app.route('/')
def homepage():
    return question('how ya doin?')

@ask.launch
def start_skill():
    welcome_message = 'Are you ready?'
    return question(welcome_message)

@ask.intent("YesIntentPhoto")
def share_headlines():
    take_picture()
    headline_msg = 'Such a beautiful smile. Gucci gang!!!!!'
    return statement(headline_msg)

@ask.intent("NoIntentPhoto")
def no_intent():
    take_picture()
    bye_text = 'Too slow I already done it.. Ha Ha..'
    return statement(bye_text)

if __name__ == '__main__':
    app.run(debug=True, port=5001)