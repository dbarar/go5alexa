from flask import Flask
from flask_ask import Ask, statement, question, session
import json
import requests
import time
import unidecode
from random import randint

import cv2

drinken_water_glasses = 0

app = Flask(__name__)
ask = Ask(app, "/")


@app.route('/')
def homepage():
    return question('how ya doin?')

@ask.launch
def start_skill():
    welcome_message = 'Drink Water reminder was started. There are 3 glasses you need to drink'
    print("Test")
    global drinken_water_glasses
    drinken_water_glasses = 3
    return question(welcome_message)

@ask.intent("YesIntent")
def share_headlines():
    headline_msg = 'There are ' + str(drinken_water_glasses) + ' glasses left. Good job, folk!'
    return statement(headline_msg)


@ask.intent("IdrankglassIntent")
def no_intent():
    global drinken_water_glasses
    if drinken_water_glasses > 0:
        drinken_water_glasses = drinken_water_glasses - 1
        bye_text = 'Nice you only have ' + str(drinken_water_glasses) + 'left'
    else:
        bye_text = 'The job is done. You have drunk enough water for today. Try some beer!'

    return statement(bye_text)


if __name__ == '__main__':
    app.run(debug=True,  port=4123)