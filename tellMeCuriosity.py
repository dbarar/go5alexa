from flask import Flask
from flask_ask import Ask, statement, question, session
import json
import requests
import time
import unidecode
from random import randint

app = Flask(__name__)
ask = Ask(app, "/")

lines = None

def get_random_curiosity():
    number = randint(0, 9)
    global lines

    if lines is None:
        f = open("curiosities", "r", encoding="utf8")
        lines = [line.rstrip('\n') for line in f]

    return lines[number]

@app.route('/')
def homepage():
    return question('how ya doin?')

@ask.launch
def start_skill():
    welcome_message = 'Hello there, would you like a curiosity?'
    return question(welcome_message)

@ask.intent("YesIntentCurio")
def share_headlines():
    curiosity = get_random_curiosity()
    headline_msg = 'Ok, here it goes, {}. Hope it impressed you!'.format(curiosity)
    return statement(headline_msg)

@ask.intent("NoIntentCurio")
def no_intent():
    bye_text = 'you woke me out of my sleep for nothing, you are a jerk. Go to hell'
    return statement(bye_text)

if __name__ == '__main__':
    app.run(debug=True, )